// Fill out your copyright notice in the Description page of Project Settings.

#include "Prototype2.h"
#include "MyPawnMovementComponent.h"


UMyPawnMovementComponent::UMyPawnMovementComponent()
{
	movementSpeed = 600;
}

void UMyPawnMovementComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Make sure that everything is still valid, and that we are allowed to move.
	if (!PawnOwner || !UpdatedComponent || ShouldSkipUpdate(DeltaTime))
	{
		return;
	}

	// Get (and then clear) the movement vector that we set in ACollidingPawn::Tick
	FVector DesiredMovementThisFrame = ConsumeInputVector().GetClampedToMaxSize(1.0f);

	float speedThisFrame;

	if (jetpacking)
	{
		speedThisFrame = jetpackMovementSpeed;
	}
	else if (!onGround)
	{
		speedThisFrame = airMovementSpeed;
	}
	else
	{
		speedThisFrame = movementSpeed;
	}

	DesiredMovementThisFrame *= speedThisFrame;


	if (jumping)
	{
		if (jumpForce <= -maxFallVelocity)
		{
			jumping = false;
		}

		FVector jump = -gravity;
		jump.Normalize();
		jump *= jumpForce;
		DesiredMovementThisFrame += jump;

		jumpForce -= gravity.Size() * DeltaTime;
	}
	else
	{
		if (!onGround)
		{
			DesiredMovementThisFrame += gravity;
		}
	}

	DesiredMovementThisFrame *= DeltaTime;

	if (!DesiredMovementThisFrame.IsNearlyZero())
	{
		FHitResult Hit;
		SafeMoveUpdatedComponent(DesiredMovementThisFrame, UpdatedComponent->GetComponentRotation(), true, Hit);

		// If we bumped into something, try to slide along it
		if (Hit.IsValidBlockingHit())
		{
			SlideAlongSurface(DesiredMovementThisFrame, 1.f - Hit.Time, Hit.Normal, Hit);
		}
	}

	RotateToGround();

	UpdateZRotation();
}

void UMyPawnMovementComponent::UpdateZRotation()
{
	APlayerController* const PC = CastChecked<APlayerController>(GetPawnOwner()->Controller);
	FRotator controlRotation = PC->GetControlRotation();

	controlRotation.Roll = 0;
	controlRotation.Pitch = 0;

	FRotator rotationDifference;

	rotationDifference = controlRotation - controllerPreviousRotation;

	this->GetOwner()->AddActorLocalRotation(rotationDifference.Quaternion());

	controllerPreviousRotation = controlRotation;
}

void UMyPawnMovementComponent::RotateToGround()
{
	FVector actorUp = this->GetOwner()->GetActorUpVector();
	FVector gravUp = -gravity;

	gravUp.Normalize();

	FQuat newRotation;

	newRotation = FQuat::FindBetweenNormals(actorUp, gravUp) * this->GetOwner()->GetActorQuat();

	this->GetOwner()->SetActorRotation(newRotation);
}

void UMyPawnMovementComponent::Jump()
{
	if (jumping != true)
	{
		jumping = true;
		jumpForce = jumpVelocity;
	}
}