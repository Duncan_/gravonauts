// Fill out your copyright notice in the Description page of Project Settings.

#include "Prototype2.h"
#include "CameraBoom.h"


UCameraBoom::UCameraBoom()
{

}

void UCameraBoom::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (bUsePawnControlRotation)
	{
		if (APawn* OwningPawn = Cast<APawn>(GetOwner()))
		{
			const FRotator PawnViewRotation = OwningPawn->GetViewRotation();
			if (PawnViewRotation != GetComponentRotation())
			{
				FRotator newRotation = PawnViewRotation;

				newRotation.Yaw = 0;
				newRotation.Roll = 0;

				//newRotation.Pitch = newRotation.Pitch - GetComponentRotation().Pitch;

				SetRelativeRotation(newRotation, false);
			}
		}
	}

	UpdateDesiredArmLocation(bDoCollisionTest, bEnableCameraLag, bEnableCameraRotationLag, DeltaTime);
}