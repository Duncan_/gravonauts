// Fill out your copyright notice in the Description page of Project Settings.

#include "Prototype2.h"
#include "CharacterQuaternionRotate.h"


void UCharacterQuaternionRotate::SetActorAndCameraQuaternionRotation(UPARAM (ref) AActor* actor, FVector impactNormal, UCameraComponent* camera, AController* controller, FRotator previousControllerRotation)
{
	//figure out the rotation
	FVector UpVector = actor->GetRootComponent()->GetUpVector();

	FVector RotationAxis = FVector::CrossProduct(UpVector, impactNormal);
	RotationAxis.Normalize();

	float DotProduct = FVector::DotProduct(UpVector, impactNormal);
	float RotationAngle = acosf(DotProduct);

	FQuat Quat = FQuat(RotationAxis, RotationAngle);
	FQuat RootQuat = actor->GetRootComponent()->GetComponentQuat();

	FQuat NewQuat = Quat * RootQuat;

	FRotator test = controller->GetControlRotation();
	test.Pitch = 0;
	test.Roll = 0;

	actor->SetActorRotation(NewQuat);
	actor->SetActorRelativeRotation(test);
}