// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PawnMovementComponent.h"
#include "MyPawnMovementComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROTOTYPE2_API UMyPawnMovementComponent : public UPawnMovementComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(Category = "Character Movement: Gravity", EditAnywhere, BlueprintReadWrite)
	FVector gravity = FVector(0);

	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere, BlueprintReadWrite)
	float movementSpeed = 600;

	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere, BlueprintReadWrite)
	float airMovementSpeed = 600;

	UPROPERTY(Category = "Character Movement: Jetpack", EditAnywhere, BlueprintReadWrite)
	float jetpackMovementSpeed = 600;

	UPROPERTY(Category = "Character Movement: Jumping", EditAnywhere, BlueprintReadWrite)
	float jumpVelocity = 600;

	UPROPERTY(Category = "Character Movement: JetPack", EditAnywhere, BlueprintReadWrite)
	float jetPackLift = 600;

	UPROPERTY(Category = "Character Movement: Jumping", EditAnywhere, BlueprintReadWrite)
	float maxFallVelocity = 600;

	UPROPERTY(Category = "Character Movement: OnGround", EditAnywhere, BlueprintReadWrite)
	bool onGround = false;
	
	FRotator controllerPreviousRotation = FRotator(0);

	UMyPawnMovementComponent();

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;

	void UpdateZRotation();
	void RotateToGround();

	UFUNCTION(BlueprintCallable, Category = "Pawn")
	void Jump();

private:

	bool jumping = false;
	float jumpForce;

	bool jetpacking = false;
};