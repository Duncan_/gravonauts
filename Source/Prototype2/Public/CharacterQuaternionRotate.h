// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "CharacterQuaternionRotate.generated.h"

/**
 * 
 */
UCLASS()
class PROTOTYPE2_API UCharacterQuaternionRotate : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	UFUNCTION(BlueprintCallable, Category = "Actor")
	static void SetActorAndCameraQuaternionRotation(AActor* actor, FVector impactNormal, UCameraComponent* camera, AController* controller, FRotator previousControllerRotation);
};
