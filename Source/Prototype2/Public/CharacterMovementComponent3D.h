// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/CharacterMovementComponent.h"
#include "CharacterMovementComponent3D.generated.h"

/**
 *
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROTOTYPE2_API UCharacterMovementComponent3D : public UCharacterMovementComponent
{
	GENERATED_BODY()
	
public:
	UPROPERTY(Category = "Character Movement (General Settings)", EditAnywhere, BlueprintReadWrite)
	FVector gravityDir;
	UPROPERTY(Category = "Character Movement (General Settings)", EditAnywhere, BlueprintReadWrite)
	float gravityStr = 980.f;

	UCharacterMovementComponent3D();

	virtual bool DoJump(bool bReplayingMoves) override;

	virtual void PhysFalling(float deltaTime, int32 Iterations) override;

	FVector GetGravity() const;

	void PerformAirControlForPathFollowing(FVector Direction, float ZDiff) override;
	void SimulateMovement(float DeltaSeconds) override;
	float GetMaxJumpHeight() const override;
	void PhysSwimming(float deltaTime, int32 Iterations) override;
	void SetPostLandedPhysics(const FHitResult& Hit) override;
	void HandleImpact(const FHitResult& Impact, float TimeSlice, const FVector& MoveDelta) override;
	void DisplayDebug(UCanvas* Canvas, const FDebugDisplayInfo& DebugDisplay, float& YL, float& YPos) override;
	void ApplyDownwardForce(float DeltaSeconds) override;
	void ApplyAccumulatedForces(float DeltaSeconds) override;
	void ComputeFloorDist(const FVector& CapsuleLocation, float LineDistance, float SweepDistance, FFindFloorResult& OutFloorResult, float SweepRadius, const FHitResult* DownwardSweepResult) const override;
	void FindFloor(const FVector& CapsuleLocation, FFindFloorResult& OutFloorResult, bool bZeroDelta, const FHitResult* DownwardSweepResult) const override;
	void K2_FindFloor(FVector CapsuleLocation, FFindFloorResult& FloorResult) const;
	bool IsWalkable(const FHitResult& Hit) const;
};
